#ifndef __CAULDRON_ROOM__
#define __CAULDRON_ROOM__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "MenuManager.h"
#include "CommandBase.h"
#include "Monster.h"

class CauldronRoom  : public cocos2d::Layer{

public:

	static cocos2d::Scene* createScene();

	virtual bool init();
	
	CREATE_FUNC(CauldronRoom);

private:

	ui::ImageView *itens[ItemBase::MAX_ITEM];
	ui::TextField *item_qtd[ItemBase::MAX_ITEM];
	int *available_item;
	static int selected_item[ItemBase::MAX_ITEM];

	KeyManager *key_manager; 
	JoyManager *joy_manager;
	
	static void cursorLeft (void *parent);
	static void cursorRight (void *parent);
	static void cursorUp(void *parent);
	static void cursorDown(void *parent);
	static void enter (void *parent);
	void showPreview(int monster_id);
	static void exit(void *parent);
	static void createMonster(void *parent);

	static void reset(void *parent);

	int checkChar();
	
	MenuManager<CauldronRoom> *manager;

	Monster *monster;
	Sprite *result;
};

#endif