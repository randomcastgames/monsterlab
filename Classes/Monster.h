#ifndef __MONSTER_H__
#define __MONSTER_H__
#include "Skill.h"
#include "Item.h"
#include "cocos2d.h"

using namespace cocos2d;

class Monster : public Sprite {

public:
	#define NUM_SKILL 4 

	typedef struct {
		char name[30];
		attribute_type att;
		SkillBase *skill;
	}monster_defaults_type;

	Monster() {
		for (int i = 0; i < NUM_SKILL; ++i)
			skill_set[i] = nullptr;

		_id = -1;

		_attribute = {0,0,0};
	}

	void setId(int id) {
		_id = id;

		switch (_id)
		{
			case BAT:
				this->initWithFile("res/Creatures/Bat.png");
			break;
			case CARNIVOUROS_PLANT:
				this->initWithFile("res/Creatures/Carnivorous_Plant.png");
				break;
			case THE_BULL:
				this->initWithFile("res/Creatures/DeBull.png");
				break;
			case HAUNTED_TREE:
				this->initWithFile("res/Creatures/Haunted_Tree.png");
				break;
			case HORN_IMP:
				this->initWithFile("res/Creatures/Horn_Imp.png");
				break;
			case SLIME:
				this->initWithFile("res/Creatures/Slime.png");
				break;
			case SNAKE:
				this->initWithFile("res/Creatures/Snake.png");
				break;
			case WASP:
				this->initWithFile("res/Creatures/Wasp.png");
				break;
		}
		/*
		if (_id >= 0)
		{
			this->setVisible(true);
			this->setPosition(Vec2(150, 150));
			this->setColor(Color3B(0, 0, 0));
		}
		else {
			this->setVisible(false);
		}*/
		this->setVisible(false);
	}
	int getId() { return _id; }
	void setAttribute(attribute_type att) { _attribute = att; }
	attribute_type *getAttribute() { return &_attribute; }

	void setSkill(int num, SkillBase* skill) {
		skill_set[num] = skill;
	}

	void cleanSkills() {
		for (int i = 0; i < NUM_SKILL; ++i) {
			delete skill_set[i];
			skill_set[i] = nullptr;
		}
	}

	SkillBase *getSkill(unsigned int num) {
		if (num < NUM_SKILL) {
			return skill_set[num];
		}
	}

	static bool generateMonster(Monster *m, int item_list[ItemBase::MAX_ITEM]);
	static int genMonsterId(int item_list[ItemBase::MAX_ITEM]);

private:

	int _id;
	attribute_type _attribute;

	SkillBase *skill_set[NUM_SKILL];

};

class Player : public Monster {
public:
	Player() {
		memset(inventory, 0, sizeof(inventory));
		inventory[ItemBase::SLIME_DROP] = 3;
	}

	static Player *getInstance() {
		if (player_instance == nullptr) player_instance = new Player();

		return player_instance;
	}

	int inventory[ItemBase::MAX_ITEM];
private:

	static Player *player_instance;
};

class Enemy : public Monster {
public:
	Enemy() {
		memset(inventory, 0, sizeof(inventory));
		this->setFlippedX(true);
	}

	static Enemy *getInstance() {
		if (enemy_instance == nullptr) enemy_instance = new Enemy();

		return enemy_instance;
	}

	int inventory[ItemBase::MAX_ITEM];
private:

	static Enemy *enemy_instance;
};

#endif