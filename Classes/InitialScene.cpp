#include "InitialScene.h"
#include "MainMenu.h"
#include "conio.h"
#include "SimpleAudioEngine.h"

cocos2d::Scene * InitialScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = InitialScene::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool InitialScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Get screen information
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// Start the key manager, later to control the arrows, enter and esc keys.
	key_manager = new KeyManager(this);
	joy_manager = new JoyManager(this, InputHandler::PLAYER_1);

	auto rootNode = CSLoader::createNode("res/InitialScene.csb");
	this->addChild(rootNode);
	this->addChild((Sprite*)rootNode->getChildByName("Sprite_1"));


	rootNode->setScaleX(visibleSize.width / rootNode->getBoundingBox().size.width);
	rootNode->setScaleY(visibleSize.height / rootNode->getBoundingBox().size.height);
	rootNode->setVisible(true);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ESCAPE, InitialScene::exitGame, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ENTER, InitialScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_KP_ENTER, InitialScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_W, InitialScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_A, InitialScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_S, InitialScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_D, InitialScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_SPACE, InitialScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_R, InitialScene::enterMenu, nullptr);


	this->schedule(schedule_selector(InitialScene::toggleTitle), 1.0f);

	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("res/SFX/mainTheme.wav", true);


	return true;
}

void InitialScene::toggleTitle(float dt)
{
	Sprite *title = (Sprite*)this->getChildByName("Sprite_1");

	if (title->isVisible())
		title->setVisible(false);
	else
		title->setVisible(true);
}

void InitialScene::exitGame(void *parent)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void InitialScene::enterMenu(void *parent)
{
	InitialScene *ini_scene = (InitialScene *)parent;

	auto scene = MainMenu::createScene();
	delete ini_scene->key_manager;
	delete ini_scene->joy_manager;
	Director::getInstance()->replaceScene(scene);
}