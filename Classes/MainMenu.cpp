#include "MainMenu.h"
#include "CauldronRoom.h"
#include "Credits.h"
#include "SimpleAudioEngine.h"

cocos2d::Scene * MainMenu::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	auto layer = MainMenu::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool MainMenu::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Get screen information
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	
	// Start the key manager, later to control the arrows, enter and esc keys.
	key_manager = new KeyManager(this);
	joy_manager = new JoyManager(this, InputHandler::PLAYER_1);
	
	auto rootNode = CSLoader::createNode("res/StartMenu.csb");
	this->addChild(rootNode);

	rootNode->setScaleX(visibleSize.width / rootNode->getBoundingBox().size.width);
	rootNode->setScaleY(visibleSize.height / rootNode->getBoundingBox().size.height);
	rootNode->setVisible(true);
	
	// PLAY BUTTON ----------------------------------------------------------------
	// get the button object from node
		ui::Button *start = (ui::Button*)rootNode->getChildByName("Start_button");
		//Define the lambda to be called when the button is pressed 
		start->addClickEventListener([=](cocos2d::Ref * pSender)
		{
			this->startGame();
		});
	// ----------------------------------------------------------------------------
	
	
	/*
	// OPTION BUTTON ----------------------------------------------------------------
	// get the button object from node
		ui::Button *options = (ui::Button*)rootNode->getChildByName("Option_button");
		//Define the lambda to be called when the button is pressed 
		options->addClickEventListener([=](cocos2d::Ref * pSender)
		{
			this->showOptions();
		});
	// ----------------------------------------------------------------------------
	*/
	
	// CREDITS BUTTON ----------------------------------------------------------------
	// get the button object from node
		ui::Button *credits = (ui::Button*)rootNode->getChildByName("Credits_button");
		//Define the lambda to be called when the button is pressed		
		credits->addClickEventListener([=](cocos2d::Ref * pSender)
		{
			this->showCredits();
		});
		
	// ----------------------------------------------------------------------------
	
	// EXIT BUTTON ----------------------------------------------------------------
	// get the button object from node
		ui::Button *exit = (ui::Button *)rootNode->getChildByName("Exit_button");
		//Define the lambda to be called when the button is pressed 
		exit->addClickEventListener([=](cocos2d::Ref * pSender)
		{
			this->exitGame();
		});
	// ----------------------------------------------------------------------------

	
	manager = new MenuManager<MainMenu>(3, 1);

	manager->newOption(start->getPosition(), nullptr, &MainMenu::startGame, 0, 0);
	//manager->newOption(options->getPosition(), nullptr, &MainMenu::showOptions, 1, 0);
	manager->newOption(credits->getPosition(), nullptr, &MainMenu::showCredits, 1, 0);
	manager->newOption(exit->getPosition(), nullptr, &MainMenu::exitGame, 2, 0);

	Sprite *select_cursor = Sprite::create("res/StartMenu_Cursor.png");
	rootNode->addChild(select_cursor);
	manager->newCursor(select_cursor, Vec2(-select_cursor->getBoundingBox().size.width * 1.15, 0), 0, 0);
	manager->updateCursor(0);
	
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_W, MainMenu::cursorUp, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_S, MainMenu::cursorDown, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_KP_ENTER, MainMenu::enter, nullptr);
		
	joy_manager->setKeyFunc(DPAD_UP, MainMenu::cursorUp, nullptr);
	joy_manager->setKeyFunc(DPAD_DOWN, MainMenu::cursorDown, nullptr);
	joy_manager->setKeyFunc(BUTTON_1, MainMenu::enter, nullptr);

	return true;
}




void MainMenu::startGame(void)
{
	auto scene = CauldronRoom::createScene();
	delete key_manager;
	delete joy_manager;
	Director::getInstance()->replaceScene(scene);
	
}

void MainMenu::showOptions(void)
{
	
}
	
void MainMenu::showCredits(void)
{
	auto scene = CreditsScene::createScene();
	delete key_manager;
	delete joy_manager;
	Director::getInstance()->replaceScene(scene);
	
}

void MainMenu::exitGame(void)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void MainMenu::cursorUp (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoMudaPosicaoDoCursoNoMenu1.wav");

	MainMenu *menu = (MainMenu*)parent;
	menu->manager->decRow(0);
}
void MainMenu::cursorDown (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoMudaPosicaoDoCursoNoMenu1.wav");

	MainMenu *menu = (MainMenu*)parent;
	menu->manager->incRow(0);
}
void MainMenu::enter (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoMudaPosicaoDoCursoNoMenu2.wav");

	MainMenu *menu = (MainMenu*)parent;
	void (MainMenu::*call)(void) = menu->manager->getCursorCall(0);
	(menu->*call)();
}


