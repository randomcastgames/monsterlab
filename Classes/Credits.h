#ifndef __CREDITS_H__
#define __CREDITS_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "MenuManager.h"
#include "CommandBase.h"

class CreditsScene : public cocos2d::Layer {

public:

	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(CreditsScene);

private:

	KeyManager *key_manager;
	JoyManager *joy_manager;

	static void back(void *parent);

	MenuManager<CreditsScene> *manager;
};

#endif