#ifndef __ITEM_H__
#define __ITEM_H__
#include "Skill.h"

enum {
	 SLIME
	,BAT
	,SNAKE
	,HORN_IMP
	,CARNIVOUROS_PLANT
	,WASP
	,HAUNTED_TREE
	,THE_BULL
};


class ItemBase {

public:

	enum {
		SLIME_DROP
		, SCALE
		, FANG
		, WING
		, BIG_EYE
		, HORN
		, BUG_WING
		, STING
		, POISON
		, VINE
		, NOSE_RING
		, WOOD
		, LEAF
		, MAX_ITEM
	};

	ItemBase(SkillBase *skill, attribute_type attribute_bonus) {
		_skill = skill;
		_bonus = attribute_bonus;
	}

	~ItemBase() {
		delete _skill;
	}

	int getId() { return _id; }
	int getMonsterId() { return _m_id; }


protected:
	int _id;
	int _m_id;
	SkillBase *_skill;
	attribute_type _bonus;

};

class WingItem : protected ItemBase {
	WingItem() : ItemBase((SkillBase*)new LeechSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = WING;
		_m_id = BAT;
	}
};
class FangItem : protected ItemBase {
	FangItem() : ItemBase((SkillBase*)new BiteSkill(), attribute_type{ 0,10,0,0 })
	{ 
		_id = FANG; 
		_m_id = -1;
	}
};
class LeafItem : protected ItemBase {
	LeafItem() : ItemBase((SkillBase*)new RazorLeafSkill(), attribute_type{ 0,10,0,0 })
	{ 
		_id = LEAF; 
		_m_id = -1;
	}
};
class VineItem : protected ItemBase {
	VineItem() : ItemBase((SkillBase*)new WJungleSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = VINE; 
		_m_id = CARNIVOUROS_PLANT;
	}
};
class WoodItem : protected ItemBase {
	WoodItem() : ItemBase((SkillBase*)new HardWoodSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = WOOD; 
		_m_id = HAUNTED_TREE;
	}
};
class HornItem : protected ItemBase {
	HornItem() : ItemBase((SkillBase*)new HornAttackSkill(), attribute_type{ 0,5,0,0 })
	{ 
		_id = HORN; 
		_m_id = -1;
	}
};
class BigEyeItem : protected ItemBase {
	BigEyeItem() : ItemBase((SkillBase*)new MeanLookSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = BIG_EYE; 
		_m_id = HORN_IMP;
	}
};
class SlimeDropItem : protected ItemBase {
	SlimeDropItem() : ItemBase((SkillBase*)new SlimeEssenceSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = SLIME_DROP; 
		_m_id = SLIME;
	}
};
class ScaleItem : protected ItemBase {
	ScaleItem() : ItemBase((SkillBase*)new UnderSkinSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = SCALE; 
		_m_id = SNAKE;
	}
};
class PoisonItem : protected ItemBase {
	PoisonItem() : ItemBase((SkillBase*)new SludgeBombSkill(), attribute_type{ 0,7,0,0 })
	{ 
		_id = POISON; 
		_m_id = -1;
	}
};
class NoseRingItem : protected ItemBase {
	NoseRingItem() : ItemBase((SkillBase*)new SneezeSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = NOSE_RING; 
		_m_id = THE_BULL;
	}
};
class StingItem : protected ItemBase {
	StingItem() : ItemBase((SkillBase*)new BadReputationSkill(), attribute_type{ 0,0,10,0 })
	{ 
		_id = STING; 
		_m_id = WASP;
	}
};


#endif