#ifndef __MAP_H__
#define __MAP_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "MenuManager.h"
#include "CommandBase.h"

#define NUM_SPOTS 5

class MapClass  : public cocos2d::Layer{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	
	CREATE_FUNC(MapClass);
	
private:
	ui::Button *buttons[NUM_SPOTS];

	KeyManager *key_manager; 
	JoyManager *joy_manager;
	
	static void cursorPrevious (void *parent);
	static void cursorNext (void *parent);
	static void enter (void *parent);
	static void backToCauldron(void *parent);

	void startBattle(void);
	
	MenuManager<MapClass> *manager;
};

#endif