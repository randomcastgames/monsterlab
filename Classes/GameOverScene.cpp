#include "GameOverScene.h"
#include "MainMenu.h"
#include "conio.h"
#include "SimpleAudioEngine.h"

cocos2d::Scene * GameOverScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameOverScene::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool GameOverScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Get screen information
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// Start the key manager, later to control the arrows, enter and esc keys.
	key_manager = new KeyManager(this);
	joy_manager = new JoyManager(this, InputHandler::PLAYER_1);

	auto rootNode = CSLoader::createNode("res/GameOver.csb");
	this->addChild(rootNode);
	this->addChild((Sprite*)rootNode->getChildByName("Sprite_1"));


	rootNode->setScaleX(visibleSize.width / rootNode->getBoundingBox().size.width);
	rootNode->setScaleY(visibleSize.height / rootNode->getBoundingBox().size.height);
	rootNode->setVisible(true);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ESCAPE, GameOverScene::exitGame, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ENTER, GameOverScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_KP_ENTER, GameOverScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_W, GameOverScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_A, GameOverScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_S, GameOverScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_D, GameOverScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_SPACE, GameOverScene::enterMenu, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_R, GameOverScene::enterMenu, nullptr);


	this->schedule(schedule_selector(GameOverScene::toggleTitle), 1.0f);

	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("res/SFX/mainTheme.wav", true);


	return true;
}

void GameOverScene::toggleTitle(float dt)
{
	Sprite *title = (Sprite*)this->getChildByName("Sprite_1");

	if (title->isVisible())
		title->setVisible(false);
	else
		title->setVisible(true);
}

void GameOverScene::exitGame(void *parent)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void GameOverScene::enterMenu(void *parent)
{
	GameOverScene *ini_scene = (GameOverScene *)parent;

	auto scene = MainMenu::createScene();
	delete ini_scene->key_manager;
	delete ini_scene->joy_manager;
	Director::getInstance()->replaceScene(scene);
}