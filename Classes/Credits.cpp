#include "Credits.h"
#include "MainMenu.h"

cocos2d::Scene * CreditsScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = CreditsScene::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool CreditsScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Get screen information
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// Start the key manager, later to control the arrows, enter and esc keys.
	key_manager = new KeyManager(this);
	joy_manager = new JoyManager(this, InputHandler::PLAYER_1);

	auto rootNode = CSLoader::createNode("res/Credits.csb");

	auto scale_x = visibleSize.width / rootNode->getBoundingBox().size.width;
	auto scale_y = visibleSize.height / rootNode->getBoundingBox().size.height;

	rootNode->setScaleX(scale_x);
	rootNode->setScaleY(scale_y);

	this->addChild(rootNode);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ESCAPE, CreditsScene::back, nullptr);

	return true;
}

void CreditsScene::back(void *parent)
{
	CreditsScene *credit_scene = (CreditsScene *)parent;

	auto scene = MainMenu::createScene();
	delete credit_scene->key_manager;
	delete credit_scene->joy_manager;
	Director::getInstance()->replaceScene(scene);
}