#include "CommandBase.h"

InputHandler *InputHandler::instance;
EventListenerKeyboard *InputHandler::eventListener;
std::map<cocos2d::EventKeyboard::KeyCode, KeyManager*> InputHandler::key_map;
std::map<int, JoyManager*> InputHandler::joy_map[4];


/*Auxiliar function*/
joystick_state::_dpad pov2JoyInput(long pov_value)
{
	joystick_state::_dpad dpad;

	memset(&dpad,false,sizeof(joystick_state::_dpad));
	
	switch (pov_value)
	{
		case 0: // Up
			dpad.up = true;
		break;
		
		case 4500: // Up +Right
			dpad.up = true;
			dpad.right = true;
		break;

		case 9000: // Right
			dpad.right = true;
		break;

		case 13500: // Right + Down
			dpad.right = true;
			dpad.down = true;
		break;

		case 18000: // Down
			dpad.down = true;
		break;

		case 22500: // Down + Left
			dpad.down = true;
			dpad.left = true;
		break;

		case 27000: // Left
			dpad.left = true;
		break;

		case 31500: // Left + Up
			dpad.left = true;
			dpad.up = true;
		break;

		default:
			break;
	}

	return dpad;
}




/**
* @brief Constructor for the InputHandler class
*
* @details In this constructor the Cocos2D events of key pressed and key released are assigned with methods from this class
*/
InputHandler::InputHandler()
{
	eventListener = EventListenerKeyboard::create();
	eventListener->onKeyPressed = pressedInput;
	eventListener->onKeyReleased = releasedInput;
	this->_eventDispatcher->addEventListenerWithFixedPriority(eventListener, 1);

	if(int num_joy = Joystick::deviceCount())
	{
		for (int i = 0; i < num_joy; ++i)
		{
			joy_vector.push_back(new Joystick(i));
			if (!joy_vector[i]->open())
			{
				delete joy_vector[i];
				joy_vector[i] = nullptr;
			}
			joy_last_state.push_back(new joystick_state);
		}

		Director *director = Director::getInstance();
		Scheduler *s = director->getScheduler();
		s->scheduleUpdate(this, 1, false);
//		this->setScheduler(s);
	//	this->scheduleUpdate();
	}


	
}

/**
* @brief Destructor for the InputHandler class
*/
InputHandler::~InputHandler()
{
	eventListener->release();
}

/**
 *  @brief Create an object of this class
 *  
 *  @return Pointer to the object of the class
 *  
 *  @details This function makes this class a singleton
 */
InputHandler * InputHandler::getInstance()
{
	if (!InputHandler::instance)
	{
		//auto layer = Layer::create();
		return InputHandler::instance = new InputHandler();
	}
	else
		return InputHandler::instance;
}


void InputHandler::update(float delta)
{
	for (int i = PLAYER_1; i < joy_vector.size(); ++i)
	{
		if (joy_vector[i] == nullptr) continue;
		
		DIJOYSTATE2 current;

		joy_vector[i]->poll(&current);
		for (int j = 0; j < SUPORTED_BUTTONS; ++j) 
		{
		
			if (joy_map[i].find(j) == joy_map[i].end()) continue;

			if ((current.rgbButtons[j] & 0x80) && (!joy_last_state[i]->buttons[j]))
			{
				joy_last_state[i]->buttons[j] = true;
				joy_map[i][j]->onKeyPressed(j);
			}
			else if ((!current.rgbButtons[j]) && (joy_last_state[i]->buttons[j]))
			{
				joy_last_state[i]->buttons[j] = false;
				joy_map[i][j]->onKeyReleased(j);
			}
		}// for (BUTTON)

		joystick_state::_dpad current_dpad = pov2JoyInput(current.rgdwPOV[0]);

		//UP
		if ((joy_map[i].find(DPAD_UP) != joy_map[i].end()) && (joy_last_state[i]->dpad.up != current_dpad.up)) {
			if (current_dpad.up)
				joy_map[i][DPAD_UP]->onKeyPressed(DPAD_UP);
			else
				joy_map[i][DPAD_UP]->onKeyReleased(DPAD_UP);
		}
		//DOWN
		if ((joy_map[i].find(DPAD_DOWN) != joy_map[i].end()) && (joy_last_state[i]->dpad.down != current_dpad.down)) {
			if (current_dpad.down)
				joy_map[i][DPAD_DOWN]->onKeyPressed(DPAD_DOWN);
			else
				joy_map[i][DPAD_DOWN]->onKeyReleased(DPAD_DOWN);
		}
		//LEFT
		if ((joy_map[i].find(DPAD_LEFT) != joy_map[i].end()) && (joy_last_state[i]->dpad.left != current_dpad.left)) {
			if (current_dpad.left)
				joy_map[i][DPAD_LEFT]->onKeyPressed(DPAD_LEFT);
			else
				joy_map[i][DPAD_LEFT]->onKeyReleased(DPAD_LEFT);
		}
		//RIGHT
		if ((joy_map[i].find(DPAD_RIGHT) != joy_map[i].end()) && (joy_last_state[i]->dpad.right != current_dpad.right)) {
			if (current_dpad.right)
				joy_map[i][DPAD_RIGHT]->onKeyPressed(DPAD_RIGHT);
			else
				joy_map[i][DPAD_RIGHT]->onKeyReleased(DPAD_RIGHT);
		}

		joy_last_state[i]->dpad = current_dpad;
					
	} // for (joy_vector.size())


}


/**
* @brief This function will serve as the event of pressing a button
*
* @details A map is used to store the @ref KeyManager for the given key in the event
*/
void InputHandler::pressedInput(EventKeyboard::KeyCode key, Event * event)
{
	if (InputHandler::key_map.find(key) != InputHandler::key_map.end())
		InputHandler::key_map[key]->onKeyPressed(key);
}

/**
* @brief This function will serve as the event of releasing a button
*
* @details A map is used to store the @ref KeyManager for the given key in the event
*/
void InputHandler::releasedInput(EventKeyboard::KeyCode key, Event * event)
{
	if (InputHandler::key_map.find(key) != InputHandler::key_map.end())
		InputHandler::key_map[key]->onKeyReleased(key);
}

/**
* @brief Adds a manager for key
* 
* @details The function verifies if there is some manager associated to the key, if there is it is removed first, and then the manager is associated with the key using a map
*/
void InputHandler::addInputManager(EventKeyboard::KeyCode key, KeyManager* manager)
{
	// Search of there is a old configuration. If does remove it.
	if (InputHandler::key_map.find(key) != InputHandler::key_map.end())
		InputHandler::key_map.erase(key);

	// Add the new key/value pair
	InputHandler::key_map[key] = manager;
}

void InputHandler::addInputManager(joy_stick_inputs key, JoyManager * manager)
{
	// Search of there is a old configuration. If does remove it.
	if (InputHandler::joy_map[manager->player].find(key) != InputHandler::joy_map[manager->player].end())
		InputHandler::joy_map[manager->player].erase(key);

	// Add the new key/value pair
	InputHandler::joy_map[manager->player][key] = manager;
}


/**
 *  @brief Remove a @ref KeyManager from the map
 */
void InputHandler::removeInputManager(KeyManager* manager)
{
	for (std::map<cocos2d::EventKeyboard::KeyCode, KeyManager*>::iterator it = key_map.begin(); it != key_map.end();) 
	{
		if (it->second == manager)
			key_map.erase(it++);
		else
			it++;
	}
}

void InputHandler::removeInputManager(JoyManager * manager)
{
	for (std::map<int, JoyManager*>::iterator it = joy_map[manager->player].begin(); it != joy_map[manager->player].end();)
	{
		if (it->second == manager)
			joy_map[manager->player].erase(it++);
		else
			it++;
	}
}




/**
* @brief This function will check the pair key/function and execute it when the key is pressed
*/
void KeyManager::onKeyPressed(EventKeyboard::KeyCode key)
{
	if (key_map.find(key) != key_map.end())
	{
		key_buffer.push_front(key);
		if(key_map[key]->onPressed != nullptr)
			key_map[key]->onPressed(this->parent);
	}
}

/**
* @brief This function will check the pair key/function and execute it when the key is released
*/
void KeyManager::onKeyReleased(EventKeyboard::KeyCode key)
{
	if (key_map.find(key) != key_map.end())
	{
		if(key_map[key]->onReleased != nullptr)
			key_map[key]->onReleased(this->parent);

		key_buffer.remove(key);

		if (key_buffer.size())
		{
			this->onKeyPressed(key_buffer.front());
			key_buffer.pop_front();
		}
	}
}


//void JoyManager::onAnalogChange(joy_stick_inputs axe, long value)
//{
//	if (joy_map.find(axe) != joy_map.end())
//	{
//		if(joy_map[axe]->analogChange != nullptr)
//			joy_map[axe]->analogChange(this, value);
//	}
//}


void JoyManager::onKeyPressed(int key)
{
	if (joy_map.find(key) != joy_map.end())
	{
		key_buffer.push_front(key);
		if (joy_map[key]->onPressed != nullptr)
			joy_map[key]->onPressed(this->parent);
	}
}

void JoyManager::onKeyReleased(int key)
{
	if (joy_map.find(key) != joy_map.end())
	{
		if (joy_map[key]->onReleased != nullptr)
			joy_map[key]->onReleased(this->parent);

		key_buffer.remove(key);

		if (key_buffer.size())
		{
			this->onKeyPressed(key_buffer.front());
			key_buffer.pop_front();
		}
	}
}
