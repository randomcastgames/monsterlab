#include "Monster.h"

Player * Player::player_instance = nullptr;
Enemy * Enemy::enemy_instance = nullptr;

bool Monster::generateMonster(Monster *m, int item_list[ItemBase::MAX_ITEM])
{
	int primary_skill;

	int id = Monster::genMonsterId(item_list);
	switch (id)
	{
	case SLIME:
		m->setAttribute({ 36,11,20,12 });
		m->setSkill(0, (SkillBase*)new SlimeEssenceSkill());
		primary_skill = ItemBase::SLIME_DROP;
		break;
	case BAT:
		m->setAttribute({ 25,15,30,7 });
		m->setSkill(0, (SkillBase*)new LeechSkill());
		primary_skill = ItemBase::WING;
		break;
	case SNAKE:
		m->setAttribute({ 30,17,30,10 });
		m->setSkill(0, (SkillBase*)new UnderSkinSkill());
		primary_skill = ItemBase::SCALE;
		break;
	case HORN_IMP:
		m->setAttribute({ 35,8,40,15 });
		m->setSkill(0, (SkillBase*)new HornAttackSkill());
		primary_skill = ItemBase::HORN;
		break;
	case CARNIVOUROS_PLANT:
		m->setAttribute({ 55,12,20,20 });
		m->setSkill(0, (SkillBase*)new WJungleSkill());
		primary_skill = ItemBase::VINE;
		break;
	case WASP:
		m->setAttribute({ 25,30,50,20 });
		m->setSkill(0, (SkillBase*)new BadReputationSkill());
		primary_skill = ItemBase::STING;
		break;
	case HAUNTED_TREE:
		m->setAttribute({ 20,18,10,40 });
		m->setSkill(0, (SkillBase*)new HardWoodSkill());
		primary_skill = ItemBase::WOOD;
		break;
	case THE_BULL:
		m->setAttribute({ 35,35,20,20 });
		m->setSkill(0, (SkillBase*)new SneezeSkill());
		primary_skill = ItemBase::NOSE_RING;
		break;
	default: return false;
	}

	m->setId(id);

	attribute_type att_tmp = *(m->getAttribute());

	att_tmp.hp += 1 * item_list[ItemBase::FANG];
	att_tmp.attack += 2 * item_list[ItemBase::HORN];
	att_tmp.defense += 1 * item_list[ItemBase::POISON];
	att_tmp.recovery += 2 * item_list[ItemBase::LEAF];

	int plus_attacks[3] = { 0,0,0 };

	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < ItemBase::MAX_ITEM; ++j) {

			if (j == primary_skill) continue;

			if ((i > 0) && (j == plus_attacks[0]))
				continue;
			else if ((i > 1) && (j == plus_attacks[1]))
				continue;

			if (item_list[plus_attacks[i]] < item_list[j])
				plus_attacks[i] = j;
		}
	}

	for (int i = 0; i < 3; ++i) {

		if (item_list[plus_attacks[i]] == 0) continue;

		if (plus_attacks[i] == primary_skill) continue;

		switch (plus_attacks[i])
		{
		case ItemBase::BIG_EYE:
			m->setSkill(i + 1, (SkillBase*)new MeanLookSkill());
			break;
		case ItemBase::SLIME_DROP:
			m->setSkill(i + 1, (SkillBase*)new SlimeEssenceSkill());
			break;
		case ItemBase::STING:
			m->setSkill(i + 1, (SkillBase*)new BadReputationSkill());
			break;
		case ItemBase::NOSE_RING:
			m->setSkill(i + 1, (SkillBase*)new SneezeSkill());
			break;
		case ItemBase::SCALE:
			m->setSkill(i + 1, (SkillBase*)new UnderSkinSkill());
			break;
		case ItemBase::VINE:
			m->setSkill(i + 1, (SkillBase*)new WJungleSkill());
			break;
		case ItemBase::WING:
			m->setSkill(i + 1, (SkillBase*)new LeechSkill());
			break;
		case ItemBase::WOOD:
			m->setSkill(i + 1, (SkillBase*)new HardWoodSkill());
			break;
		case ItemBase::BUG_WING:
			m->setSkill(i + 1, (SkillBase*)new SilverWindSkill());
			break;
		case ItemBase::FANG:
			m->setSkill(i + 1, (SkillBase*)new BiteSkill());
			break;
		case ItemBase::LEAF:
			m->setSkill(i + 1, (SkillBase*)new RazorLeafSkill());
			break;
		case ItemBase::HORN:
			m->setSkill(i + 1, (SkillBase*)new HornAttackSkill());
			break;
		case ItemBase::POISON:
			m->setSkill(i + 1, (SkillBase*)new SludgeBombSkill());
			break;
		}

	}
	return true;
}

int Monster::genMonsterId(int item_list[ItemBase::MAX_ITEM]) 
{
	int index_sort_list[ItemBase::MAX_ITEM];

	for (int i = 0; i < ItemBase::MAX_ITEM; ++i)
		index_sort_list[i] = i;

	for (int i = 0; i < ItemBase::MAX_ITEM - 1; ++i) {
		for (int j = i + 1; j < ItemBase::MAX_ITEM; ++j) {
			if (item_list[index_sort_list[i]] < item_list[index_sort_list[j]]) {
				int tmp = index_sort_list[j];
				index_sort_list[j] = index_sort_list[i];
				index_sort_list[i] = tmp;
			}
		}
	}

	for (int i = 0; i < ItemBase::MAX_ITEM; ++i)
	{
		switch (index_sort_list[i])
		{
		case ItemBase::SLIME_DROP:
			if (item_list[ItemBase::SLIME_DROP] >= 3)
				return SLIME;
			else break;
		case ItemBase::SCALE:
			if ((item_list[ItemBase::SCALE] >= 1) && (item_list[ItemBase::FANG] >= 1) && (item_list[ItemBase::POISON] >= 1))
				return SNAKE;
			else break;
		case ItemBase::WING:
			if ((item_list[ItemBase::WING] >= 2) && (item_list[ItemBase::FANG] >= 1))
				return BAT;
			else break;
		case ItemBase::BIG_EYE:
			if ((item_list[ItemBase::HORN] >= 2) && (item_list[ItemBase::BIG_EYE] >= 1))
				return HORN_IMP;
			else break;
		case ItemBase::STING:
			if ((item_list[ItemBase::STING] >= 1) && (item_list[ItemBase::BUG_WING] >= 1) && (item_list[ItemBase::POISON] >= 1))
				return WASP;
			else break;
		case ItemBase::VINE:
			if ((item_list[ItemBase::VINE] >= 1) && (item_list[ItemBase::LEAF] >= 1) && (item_list[ItemBase::FANG] >= 1))
				return CARNIVOUROS_PLANT;
			else break;
		case ItemBase::WOOD:
			if ((item_list[ItemBase::WOOD] >= 2) && (item_list[ItemBase::LEAF] >= 1))
				return HAUNTED_TREE;
			else break;
		case ItemBase::NOSE_RING:
			if ((item_list[ItemBase::NOSE_RING] >= 1) && (item_list[ItemBase::WING] >= 1) && (item_list[ItemBase::POISON] >= 1) && (item_list[ItemBase::HORN] >= 1))
				return THE_BULL;
			else break;
		}
	}
	return -1;
}