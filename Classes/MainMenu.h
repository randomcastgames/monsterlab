#ifndef __MAIN_MENU_H__
#define __MAIN_MENU_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "MenuManager.h"
#include "CommandBase.h"

class MainMenu : public cocos2d::Layer{

public:

	static cocos2d::Scene* createScene();

	virtual bool init();
	
	CREATE_FUNC(MainMenu);

private:

	KeyManager *key_manager; 
	JoyManager *joy_manager;

	void startGame(void);
	void showOptions(void);
	void showCredits(void);
	void exitGame(void);
	
	static void cursorUp (void *parent);
	static void cursorDown (void *parent);
	static void enter (void *parent);
	
	MenuManager<MainMenu> *manager;
};

#endif