#ifndef __SKILL_H__
#define __SKILL_H__


typedef struct{
	int hp;
	int attack;
	int recovery;
	int defense;
}attribute_type;


class SkillBase{

public:

	SkillBase(int dmg, int lvl, int cooldown)
	{
		_base_damage = dmg;
		_lvl = lvl;
		_cooldown = cooldown;
	}
	
	virtual void applyDamage(attribute_type *player,attribute_type *enemy)
	{
		int damage = (_base_damage + player->attack) - enemy->defense;
		if (damage <= 0) damage = 1;

		enemy->hp -= damage;
	}
	
	int getLevel(){return _lvl;}
	int getCooldown(){return _cooldown;}

protected:
	
	int _lvl;
	int _base_damage;
	int _cooldown;
	
};

class LeechSkill : protected SkillBase{
	
public:	
	LeechSkill(): SkillBase(2, 0, 0){
		
	} 
	
	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		player->hp += enemy->hp * 0.05;
	}

};

class BiteSkill : protected SkillBase{

public:	
	BiteSkill(): SkillBase(3, 0, 10){
		
	} 
};

class RazorLeafSkill : protected SkillBase{

public:
	RazorLeafSkill(): SkillBase(3, 0, 20){
		
	} 
};

class WJungleSkill : protected SkillBase{
	
public:	
	WJungleSkill (): SkillBase(2, 0, 10){
		
	} 

	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		player->attack += player->attack * 0.10;
	}
};

class HardWoodSkill : protected SkillBase{
	
public:	
	HardWoodSkill (): SkillBase(3, 0, 20){
		
	} 

	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		player->defense += player->defense * 0.50;
	}
};

class HornAttackSkill : protected SkillBase{
	
public:	
	HornAttackSkill (): SkillBase(5, 0, 10){
		
	} 
};

class MeanLookSkill : protected SkillBase{
	
public:	
	MeanLookSkill() : SkillBase(4, 0, 10){
		
	} 

	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		enemy->attack -= enemy->attack * 0.10;
	}
};

class SlimeEssenceSkill : protected SkillBase{
	
public:	
	SlimeEssenceSkill() : SkillBase(5, 0, 40){
		
	} 

	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		enemy->recovery -= enemy->recovery * 0.10;
	}
};

class UnderSkinSkill : protected SkillBase{
	
public:	
	UnderSkinSkill() : SkillBase(1, 0, 10){
		
	} 
};

class SludgeBombSkill : protected SkillBase{
	
public:	
	SludgeBombSkill() : SkillBase(2, 0, 0){
		
	} 
};

class SneezeSkill : protected SkillBase{

public:
	SneezeSkill (): SkillBase(6, 0, 10){
		
	} 

	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		player->hp -= player->hp *0.20;
		player->attack += player->attack *0.10;
	}
};

class BadReputationSkill : protected SkillBase {

public:
	BadReputationSkill() : SkillBase(4, 0, 20) {

	}

	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		enemy->defense -= enemy->defense *0.10;
	}
};

class SilverWindSkill : protected SkillBase {

public:
	SilverWindSkill() : SkillBase(3, 0, 0) {

	}

	void applyDamage(attribute_type *player, attribute_type *enemy) override
	{
		SkillBase::applyDamage(player, enemy);
		enemy->recovery -= enemy->recovery *0.10;
	}
};


#endif