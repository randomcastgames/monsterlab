#ifndef __INITIAL_SCENE_H__
#define __INITIAL_SCENE_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "MenuManager.h"
#include "CommandBase.h"

class InitialScene : public cocos2d::Layer {

public:

	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(InitialScene);

private:

	KeyManager *key_manager;
	JoyManager *joy_manager;

	void toggleTitle(float dt);
	static void enterMenu(void *parent);

	static void exitGame(void *parent);

	MenuManager<InitialScene> *manager;
};

#endif