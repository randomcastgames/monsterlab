#include "BattleScene.h"
#include "CauldronRoom.h"
#include "SimpleAudioEngine.h"
#include "GameOverScene.h"

cocos2d::Scene * BattleScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = BattleScene::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool BattleScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Get screen information
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// Start the key manager, later to control the arrows, enter and esc keys.
	key_manager = new KeyManager(this);
	joy_manager = new JoyManager(this, InputHandler::PLAYER_1);

	auto rootNode = CSLoader::createNode("res/BattleScene.csb");

	auto scale_x = visibleSize.width / rootNode->getBoundingBox().size.width;
	auto scale_y = visibleSize.height / rootNode->getBoundingBox().size.height;

	rootNode->setScaleX(scale_x);
	rootNode->setScaleY(scale_y);

	this->addChild(rootNode);
	rootNode->setVisible(true);

	// Life bars -----------------------------------------------------------------

	player_lifebar = (ui::LoadingBar*)rootNode->getChildByName("PlayerLifeBar");
	enemy_lifebar = (ui::LoadingBar*)rootNode->getChildByName("EnemyLifeBar");

	enemy_cooldown = 100;

	player_monster = (Monster*)Player::getInstance();
	this->addChild(player_monster);
	player_monster->setScaleX(scale_x);
	player_monster->setScaleY(scale_y);
	player_monster->setPosition(visibleSize.width * 0.32, visibleSize.height * 0.30);
	player_monster->setVisible(true);

	enemy_monster = (Monster*)Enemy::getInstance();
	this->addChild(enemy_monster);
	enemy_monster->setScaleX(scale_x);
	enemy_monster->setScaleY(scale_y);
	enemy_monster->setPosition(visibleSize.width * 0.74, visibleSize.height * 0.30);
	enemy_monster->setVisible(true);
	
	player_lifebar = (ui::LoadingBar*)rootNode->getChildByName("PlayerLifeBar");
	player_life_reference = player_monster->getAttribute()->hp;
	enemy_lifebar = (ui::LoadingBar*)rootNode->getChildByName("EnemyLifeBar");
	enemy_life_reference = enemy_monster->getAttribute()->hp;

	// Attack Buttons -------------------------------------------------------------
	// get the button object from node

	for (int i = 0; i < NUM_ATTACK; ++i) {
		char name[50];
		sprintf(name, "Attack_%d", i + 1);
		attack_button[i] = (ui::Button*)rootNode->getChildByName(name);

		sprintf(name, "AttackBar_%d", i + 1);
		attack_pbar[i] = (ui::LoadingBar*)rootNode->getChildByName(name);
	}

	enemy_attack_pbar = (ui::LoadingBar*)rootNode->getChildByName("AttackBarEnemy");

	ui::Button *start = (ui::Button*)rootNode->getChildByName("Start_button");
	//Define the lambda to be called when the button is pressed 
	/*
	start->addClickEventListener([=](cocos2d::Ref * pSender)
	{
		this->startGame();
	});
	*/
	// ----------------------------------------------------------------------------

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_1, BattleScene::activateAttack1, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_2, BattleScene::activateAttack2, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_3, BattleScene::activateAttack3, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_4, BattleScene::activateAttack4, nullptr);

	joy_manager->setKeyFunc(BUTTON_1,BattleScene::activateAttack1, nullptr);
	joy_manager->setKeyFunc(BUTTON_2,BattleScene::activateAttack2, nullptr);
	joy_manager->setKeyFunc(BUTTON_3,BattleScene::activateAttack3, nullptr);
	joy_manager->setKeyFunc(BUTTON_4,BattleScene::activateAttack4, nullptr);

	this->scheduleUpdate();

	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("res/SFX/battleTheme.wav", true);

	return true;
}

void BattleScene::activateAttack1(void *parent) {
	BattleScene *scene = (BattleScene*)parent;
	scene->activateAttack(0);
}

void BattleScene::activateAttack2(void *parent) {
	BattleScene *scene = (BattleScene*)parent;
	scene->activateAttack(1);
}

void BattleScene::activateAttack3(void *parent) {
	BattleScene *scene = (BattleScene*)parent;
	scene->activateAttack(2);
}

void BattleScene::activateAttack4(void *parent) {
	BattleScene *scene = (BattleScene*)parent;
	scene->activateAttack(3);
}

void BattleScene::activateAttack(int num)
{
	SkillBase *skill = player_monster->getSkill(num);
	if (skill == nullptr) return;

	if (attack_pbar[num]->getPercent() < 100) return;

	skill->applyDamage(player_monster->getAttribute(),enemy_monster->getAttribute());

	enemy_lifebar->setPercent(((float)enemy_monster->getAttribute()->hp /(float)enemy_life_reference) * 100);
	player_lifebar->setPercent(((float)player_monster->getAttribute()->hp/ (float)player_life_reference) *100);

	used_skill = skill;

	for (int i = 0; i < NUM_ATTACK; ++i)
		attack_pbar[i]->setPercent(0);
}

void BattleScene::update(float delta)
{
	Node::update(delta);

	for (int i = 0; i < NUM_ATTACK; ++i) {

		if (player_monster->getSkill(i) == nullptr) {
			attack_pbar[i]->setPercent(0);
			continue;
		}

		if(attack_pbar[i]->getPercent() < 100)
			attack_pbar[i]->setPercent(attack_pbar[i]->getPercent() + ((used_skill->getCooldown() + player_monster->getAttribute()->recovery) * delta));
	}

	if (enemy_attack_pbar->getPercent() < 100) {
		enemy_attack_pbar->setPercent(enemy_attack_pbar->getPercent() + ((enemy_skill->getCooldown() + enemy_monster->getAttribute()->recovery) * delta));
	}
	else
	{
		int selected_skill;
		do {
			selected_skill = RandomHelper::random_int(0, 3);
			enemy_skill = enemy_monster->getSkill(selected_skill);
		} while (enemy_skill == nullptr);

		enemy_skill->applyDamage(enemy_monster->getAttribute(), player_monster->getAttribute());

		enemy_lifebar->setPercent(((float)enemy_monster->getAttribute()->hp /(float)enemy_life_reference) * 100);
		player_lifebar->setPercent(((float)player_monster->getAttribute()->hp/ (float)player_life_reference) *100);

		enemy_attack_pbar->setPercent(0);
	}
	
	if ((enemy_lifebar->getPercent() <= 0) || ((player_lifebar->getPercent() <= 0))) {

		if (player_lifebar->getPercent() > 0) {

			for (int i = 0; i < ItemBase::MAX_ITEM; ++i)
				Player::getInstance()->inventory[i] += Enemy::getInstance()->inventory[i];
		}
		else {
			
			if (Monster::genMonsterId(Player::getInstance()->inventory) == -1) {

				Player::getInstance()->inventory[ItemBase::SLIME_DROP] = 3;

				auto scene = GameOverScene::createScene();
				delete this->key_manager;
				delete this->joy_manager;
				Director::getInstance()->replaceScene(scene);
				return;
			}

		}

		auto scene = CauldronRoom::createScene();
		delete key_manager;
		delete joy_manager;
		player_monster->cleanSkills();
		enemy_monster->cleanSkills();
		Director::getInstance()->replaceScene(scene);
	}

}