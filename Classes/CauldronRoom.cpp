#include "CauldronRoom.h"
#include "MapClass.h"
#include "MainMenu.h"
#include "Item.h"
#include "SimpleAudioEngine.h"


int CauldronRoom::selected_item[ItemBase::MAX_ITEM];

cocos2d::Scene * CauldronRoom::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	auto layer = CauldronRoom::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool CauldronRoom::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Get screen information
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	
	// Start the key manager, later to control the arrows, enter and esc keys.
	key_manager = new KeyManager(this);
	joy_manager = new JoyManager(this, InputHandler::PLAYER_1);
	
	auto rootNode = CSLoader::createNode("res/CouldronRoom.csb");
	this->addChild(rootNode);

	result = (Sprite*)rootNode->getChildByName("Ritual_Result");

	rootNode->setScaleX(visibleSize.width / rootNode->getBoundingBox().size.width);
	rootNode->setScaleY(visibleSize.height / rootNode->getBoundingBox().size.height);
	rootNode->setVisible(true);
	

	if (Player::getInstance()->getAttribute()->hp <= 0) {
		memset(selected_item, 0, sizeof(selected_item));
		Player::getInstance()->setVisible(false);
	}

	available_item = Player::getInstance()->inventory;

	
	
	manager = new MenuManager<CauldronRoom>(5, 3);

	monster = (Monster*)Player::getInstance();
	this->addChild(monster);

	for (int i = 0; i < 5; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if ((i == 4) && (j == 1)) break;

			char item_name[10];
			sprintf(item_name, "item%d", i*3 + j + 1);

			itens[i * 3 + j] = (ui::ImageView *)rootNode->getChildByName(item_name);

			manager->newOption(itens[i * 3 + j]->getPosition(), nullptr, nullptr, i, j);

			char qtd_string[10];
			sprintf(qtd_string, "qtd%d", i * 3 + j + 1);
			item_qtd[i * 3 + j] = (ui::TextField*)rootNode->getChildByName(qtd_string);
			item_qtd[i * 3 + j]->setText(CCString::createWithFormat("x%d", available_item[i * 3 + j])->getCString());
		}
	}

	Sprite *select_cursor = (Sprite *)rootNode->getChildByName("select_cursor");
	this->addChild(select_cursor);
	select_cursor->setVisible(true);
	manager->newCursor(select_cursor, Vec2(0,-itens[0]->getBoundingBox().size.height / 1.5), 0, 0);
	manager->updateCursor(0);
	
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_LEFT_ARROW, CauldronRoom::cursorLeft, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_A, CauldronRoom::cursorLeft, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_RIGHT_ARROW, CauldronRoom::cursorRight, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_D, CauldronRoom::cursorRight, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_UP_ARROW, CauldronRoom::cursorUp, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_W, CauldronRoom::cursorUp, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_DOWN_ARROW, CauldronRoom::cursorDown, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_S, CauldronRoom::cursorDown, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ENTER, CauldronRoom::enter, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_KP_ENTER, CauldronRoom::enter, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_SPACE, CauldronRoom::createMonster, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ESCAPE, CauldronRoom::exit, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_X, CauldronRoom::reset, nullptr);

	joy_manager->setKeyFunc(DPAD_UP, CauldronRoom::cursorUp, nullptr);
	joy_manager->setKeyFunc(DPAD_DOWN, CauldronRoom::cursorDown, nullptr);
	joy_manager->setKeyFunc(DPAD_LEFT, CauldronRoom::cursorLeft, nullptr);
	joy_manager->setKeyFunc(DPAD_RIGHT, CauldronRoom::cursorRight, nullptr);
	joy_manager->setKeyFunc(BUTTON_1, CauldronRoom::enter, nullptr);
	joy_manager->setKeyFunc(BUTTON_4, CauldronRoom::createMonster, nullptr);
	joy_manager->setKeyFunc(BUTTON_9, CauldronRoom::exit, nullptr);

	if (Player::getInstance()->getAttribute()->hp > 0)
		showPreview(Player::getInstance()->getId());

	CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("res/SFX/mainTheme.wav", true);

	return true;
}

void CauldronRoom::cursorLeft (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoMudaPosicaoDoCursoNoMenu1.wav");

	CauldronRoom *menu = (CauldronRoom*)parent;
	menu->manager->decCollum(0);
}
void CauldronRoom::cursorRight (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoMudaPosicaoDoCursoNoMenu1.wav");

	CauldronRoom *menu = (CauldronRoom*)parent;
	menu->manager->incCollum(0);
}
void CauldronRoom::cursorUp(void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoMudaPosicaoDoCursoNoMenu1.wav");

	CauldronRoom *menu = (CauldronRoom*)parent;
	menu->manager->decRow(0);
}
void CauldronRoom::cursorDown(void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoMudaPosicaoDoCursoNoMenu1.wav");

	CauldronRoom *menu = (CauldronRoom*)parent;
	menu->manager->incRow(0);
}
void CauldronRoom::enter (void *parent)
{
	CauldronRoom *menu = (CauldronRoom*)parent;
	int current_item = (int)menu->manager->getCursorPos(0).x*3+(int)menu->manager->getCursorPos(0).y;

	if (menu->available_item[current_item])
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/SomQuandoJogaOItemNaCaldeira.wav");

		--(menu->available_item[current_item]);
		menu->item_qtd[current_item]->setText(CCString::createWithFormat("x%d", menu->available_item[current_item])->getCString());
		++(menu->selected_item[current_item]);

		int m_id = Monster::genMonsterId(menu->selected_item);
		if (m_id == -1) {
			menu->result->setVisible(false);
			return;
		}

		menu->monster->setId(m_id);

		menu->showPreview(m_id);


	}

	//void (CauldronRoom::*call)(void) = menu->manager->getCursorCall(0);
	//(menu->*call)();
}

void CauldronRoom::showPreview(int monster_id) {
	switch (monster_id)
	{
	case BAT:
		result->initWithFile("res/Scenarios/Ritual_Result_Bat.png");
		break;
	case CARNIVOUROS_PLANT:
		result->initWithFile("res/Scenarios/Ritual_Result_CarnivorousPlant.png");
		break;
	case THE_BULL:
		result->initWithFile("res/Scenarios/Ritual_Result_DeBull.png");
		break;
	case HAUNTED_TREE:
		result->initWithFile("res/Scenarios/Ritual_Result_HauntedTree.png");
		break;
	case HORN_IMP:
		result->initWithFile("res/Scenarios/Ritual_Result_HornImp.png");
		break;
	case SLIME:
		result->initWithFile("res/Scenarios/Ritual_Result_Slime.png");
		break;
	case SNAKE:
		result->initWithFile("res/Scenarios/Ritual_Result_Snake.png");
		break;
	case WASP:
		result->initWithFile("res/Scenarios/Ritual_Result_Wasp.png");
		break;
	default:return;
	}

	result->setVisible(true);

}

void CauldronRoom::exit(void *parent)
{
	CauldronRoom *menu = (CauldronRoom*)parent;

	auto scene = MainMenu::createScene();
	delete menu->key_manager;
	delete menu->joy_manager;
	Director::getInstance()->replaceScene(scene);
}

void CauldronRoom::createMonster(void *parent)
{
	CauldronRoom *menu = (CauldronRoom*)parent;

	if (Monster::generateMonster(menu->monster, menu->selected_item))
	{
		CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/SomDepoisDeJogarItemNaCaldeira.wav");

		auto scene = MapClass::createScene();
		delete menu->key_manager;
		delete menu->joy_manager;
		Director::getInstance()->replaceScene(scene);
	}
}

void CauldronRoom::reset(void *parent)
{
	CauldronRoom *menu = (CauldronRoom*)parent;

	for (int i = 0; i < ItemBase::MAX_ITEM; ++i)
	{
		if(menu->selected_item[i]>0)
			menu->available_item[i] += menu->selected_item[i];

		menu->item_qtd[i]->setText(CCString::createWithFormat("x%d", menu->available_item[i])->getCString());
		menu->selected_item[i] = 0;
	}

	menu->monster->setId(-1);
	menu->result->setVisible(false);
}