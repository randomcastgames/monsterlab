#ifndef __COMMAND_BASE_H__
#define __COMMAND_BASE_H__

#include "cocos2d.h"
#include <functional>
#include <map>
#include <vector>
#include "joystick.h"

using namespace cocos2d;
class KeyManager;
class JoyManager;

typedef enum {
	BUTTON_1,
	BUTTON_2,
	BUTTON_3,
	BUTTON_4,
	BUTTON_5,
	BUTTON_6,
	BUTTON_7,
	BUTTON_8,
	BUTTON_9,
	BUTTON_10,
	BUTTON_11,
	BUTTON_12,
	BUTTON_13,
	BUTTON_14,
	BUTTON_15,
	DPAD_UP,
	DPAD_DOWN,
	DPAD_RIGHT,
	DPAD_LEFT,
	ANALOG_1_Y,
	ANALOG_1_X
}joy_stick_inputs;

#define SUPORTED_BUTTONS 15
typedef struct {
	bool buttons[SUPORTED_BUTTONS];
	struct _dpad {
		bool up;
		bool down;
		bool left;
		bool right;
	}dpad;
}joystick_state;



/**
*  @brief Class to control input events
*/
class InputHandler : Node
{
	

public:

	typedef enum 
	{
		PLAYER_1,
		PLAYER_2,
		PLAYER_3,
		PLAYER_4
	}player_enum_type;



	InputHandler();
	~InputHandler();

	static InputHandler* getInstance();

	// Functions to update the keys status
	static void pressedInput(EventKeyboard::KeyCode key, Event* event);
	static void releasedInput(EventKeyboard::KeyCode key, Event* event);

	void addInputManager(EventKeyboard::KeyCode key, KeyManager* manager);
	void addInputManager(joy_stick_inputs key, JoyManager* manager);
	void removeInputManager(KeyManager* manager);
	void removeInputManager(JoyManager* manager);

	void update(float delta) override;
	
private:
	static std::map<cocos2d::EventKeyboard::KeyCode, KeyManager*> key_map;
	static std::map<int, JoyManager*> joy_map[4];

	std::vector<Joystick*>joy_vector;
	std::vector<joystick_state*>joy_last_state;
	static InputHandler *instance;
	static EventListenerKeyboard *eventListener;
};

/**
* @brief Class to link a function to a key
*/
class KeyManager
{
public:
	KeyManager(void *_parent) :parent(_parent) {}
	~KeyManager() { 
		InputHandler::getInstance()->removeInputManager(this); 

		for (std::map<cocos2d::EventKeyboard::KeyCode, execute_type*>::iterator it = key_map.begin(); it != key_map.end();)
			key_map.erase(it++);
		
		for (std::list<cocos2d::EventKeyboard::KeyCode>::iterator it = key_buffer.begin(); it != key_buffer.end();)
			key_buffer.erase(it++);
	
	}

	/**
	* @brief Function to create a connection between a key and a press and release function
	*/
	void setKeyFunc(EventKeyboard::KeyCode key, std::function<void(void *_parent)>on_press, std::function<void(void *_parent)>on_release)
	{

		execute_type *exec = new execute_type{on_press,on_release};
		
		key_map[key] = exec;

		InputHandler::getInstance()->addInputManager(key, this);
	}


	void onKeyPressed(EventKeyboard::KeyCode key);
	void onKeyReleased(EventKeyboard::KeyCode key);
private:

	typedef struct{
		std::function<void(void *_parent)> onPressed;
		std::function<void(void *_parent)> onReleased;
	}execute_type;

	std::map<cocos2d::EventKeyboard::KeyCode, execute_type*> key_map;
	std::list<cocos2d::EventKeyboard::KeyCode> key_buffer;
	
	void *parent;
};


class JoyManager 
{
public:

	JoyManager(void *_parent, int player_num) :parent(_parent), player(player_num){}
	~JoyManager() {
		InputHandler::getInstance()->removeInputManager(this); 
	
		for (std::map<int, JoyManager::execute_type*>::iterator it = joy_map.begin(); it != joy_map.end();)
			joy_map.erase(it++);

		for (std::list<int>::iterator it = key_buffer.begin(); it != key_buffer.end();)
			key_buffer.erase(it++);
	}

	void setKeyFunc(joy_stick_inputs key, std::function<void(void *_parent)>on_press, std::function<void(void *_parent)>on_release)
	{

		execute_type *exec = new execute_type{ on_press,on_release };

		joy_map[key] = exec;

		InputHandler::getInstance()->addInputManager(key, this);
	}

	//void onAnalogChange(joy_stick_inputs axe);
	void onKeyPressed(int key);
	void onKeyReleased(int key);

	
	std::list<int> key_buffer;
	int player;
private:

	typedef struct {
		//std::function<void(void *_parent)> analogChange;
		std::function<void(void *_parent)> onPressed;
		std::function<void(void *_parent)> onReleased;
	}execute_type;

	std::map<int, JoyManager::execute_type*> joy_map;

	void *parent;
};

#endif