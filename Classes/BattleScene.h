#ifndef __BATTLE_SCENE_H__
#define __BATTLE_SCENE_H__

#include "cocos2d.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "CommandBase.h"
#include "Monster.h"

class BattleScene : public cocos2d::Layer {

public:

	static cocos2d::Scene* createScene();

	virtual bool init();
	virtual void update(float delta) override;

	CREATE_FUNC(BattleScene);

private:

	KeyManager *key_manager;
	JoyManager *joy_manager;

	#define NUM_ATTACK 4
	ui::Button *attack_button[NUM_ATTACK];
	ui::LoadingBar *attack_pbar[NUM_ATTACK];

	ui::LoadingBar *enemy_attack_pbar;

	int enemy_cooldown;

	Monster *player_monster;
	Monster *enemy_monster;

	SkillBase *used_skill;
	SkillBase *enemy_skill;

	ui::LoadingBar *player_lifebar,*enemy_lifebar;
	int player_life_reference, enemy_life_reference;

	static void activateAttack1(void *parent);
	static void activateAttack2(void *parent);
	static void activateAttack3(void *parent);
	static void activateAttack4(void *parent);
	void activateAttack(int num);
};

#endif