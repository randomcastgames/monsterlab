#include "MapClass.h"
#include "BattleScene.h"
#include "CauldronRoom.h"
#include "SimpleAudioEngine.h"


int monsters_item_list[][ItemBase::MAX_ITEM] = {
	 { 0,0,1,2,0,0,0,0,0,0,0,0,0 }
	,{ 3,0,0,0,0,0,0,0,0,0,0,0,0 }
	,{ 0,0,1,2,0,0,0,0,0,0,0,0,0 }
	,{ 0,1,1,0,0,0,0,0,1,0,0,0,0 }
	,{ 0,1,1,0,0,0,0,0,1,0,0,0,0 }
	,{ 0,0,0,0,1,2,0,0,0,0,0,0,0 }
	,{ 0,0,0,0,1,2,0,0,0,0,0,0,0 }
	,{ 0,0,1,0,0,0,0,0,0,1,0,0,1 }
	,{ 0,0,1,0,0,0,0,0,0,1,0,0,1 }
	,{ 0,0,0,0,0,0,1,1,1,0,0,0,0 }
	,{ 0,0,0,0,0,0,1,1,1,0,0,0,0 }
	,{ 0,0,0,0,0,0,0,0,0,0,0,2,1 }
	,{ 0,0,0,0,0,0,0,0,0,0,0,2,1 }
	,{ 0,0,0,1,0,1,0,0,1,0,1,0,0 }
	,{ 0,0,0,1,0,1,0,0,1,0,1,0,0 }
};

cocos2d::Scene * MapClass::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	auto layer = MapClass::create();
	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool MapClass::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Get screen information
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	
	// Start the key manager, later to control the arrows, enter and esc keys.
	key_manager = new KeyManager(this);
	joy_manager = new JoyManager(this, InputHandler::PLAYER_1);
	
	auto rootNode = CSLoader::createNode("res/Map.csb");
	this->addChild(rootNode);

	rootNode->setScaleX(visibleSize.width / rootNode->getBoundingBox().size.width);
	rootNode->setScaleY(visibleSize.height / rootNode->getBoundingBox().size.height);
	rootNode->setVisible(true);
	
	manager = new MenuManager<MapClass>(1, NUM_SPOTS);

	for (int i = 0; i < NUM_SPOTS; ++i)
	{
		char item_name[30];
		sprintf(item_name,"Button_1_%d",i);

		buttons[i] = (ui::Button *)rootNode->getChildByName(item_name);
		
		manager->newOption(buttons[i]->getPosition(), nullptr, &MapClass::startBattle, 0, i);
	}

	ui::Button * back_button = (ui::Button *)rootNode->getChildByName("back_button");

	Sprite *select_cursor = (Sprite *)rootNode->getChildByName("map_cursor");
	manager->newCursor(select_cursor, Vec2(0, select_cursor->getBoundingBox().size.height * 0.6), 0, 0);
	manager->updateCursor(0);
	
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_LEFT_ARROW, MapClass::cursorPrevious, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_A, MapClass::cursorPrevious, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_RIGHT_ARROW, MapClass::cursorNext, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_D, MapClass::cursorNext, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ENTER, MapClass::enter, nullptr);
	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_KP_ENTER, MapClass::enter, nullptr);

	key_manager->setKeyFunc(EventKeyboard::KeyCode::KEY_ESCAPE, MapClass::backToCauldron, nullptr);

	joy_manager->setKeyFunc(DPAD_UP, MapClass::cursorPrevious, nullptr);
	joy_manager->setKeyFunc(DPAD_DOWN, MapClass::cursorNext, nullptr);
	joy_manager->setKeyFunc(BUTTON_1, MapClass::enter, nullptr);

	return true;
	
	
}

void MapClass::cursorPrevious (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoSelecionaOLugar2.wav");
	MapClass *menu = (MapClass*)parent;
	menu->manager->decCollum(0);
}
void MapClass::cursorNext (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoSelecionaOLugar2.wav");
	MapClass *menu = (MapClass*)parent;
	menu->manager->incCollum(0);
}
void MapClass::enter (void *parent)
{
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("res/SFX/somDeQuandoSelecionaOLugar3.wav");

	MapClass *menu = (MapClass*)parent;
	int current_item = (int)menu->manager->getCursorPos(0).y;

	if (menu->buttons[current_item]->isEnabled())
	{
		menu->buttons[current_item]->setEnabled(false);
	}

	Monster::generateMonster((Monster*)Enemy::getInstance(), monsters_item_list[current_item]);

	memcpy(Enemy::getInstance()->inventory, monsters_item_list[current_item], sizeof(Enemy::getInstance()->inventory));

	void (MapClass::*call)(void) = menu->manager->getCursorCall(0);
	(menu->*call)();
}

void MapClass::startBattle(void)
{
	auto scene = BattleScene::createScene();
	delete key_manager;
	delete joy_manager;
	Director::getInstance()->replaceScene(scene);
}

void MapClass::backToCauldron(void *parent)
{
	MapClass *menu = (MapClass*)parent;

	auto scene = CauldronRoom::createScene();
	delete menu->key_manager;
	delete menu->joy_manager;
	Director::getInstance()->replaceScene(scene);
}