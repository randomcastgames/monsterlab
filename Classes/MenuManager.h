#ifndef __MENU_MANAGER_H__
#define __MENU_MANAGER_H__

#include "cocos2d.h"
#include <vector>

using namespace cocos2d;
template <class T>
class MenuManager {

public:

	typedef struct {
		Vec2 pos;
		void *data;
		void(T::*call)(void);
	}option_type;

	

	MenuManager(unsigned int num_rows, unsigned int num_collum)
	{
		option_matrix.resize(num_rows);
		for (int i = 0; i < option_matrix.size(); ++i) 
		{
			option_matrix[i].resize(num_collum,nullptr);
		}
	}
	~MenuManager() {};

	bool newOption(Vec2 pos,void *data ,void (T::*call)(void) , unsigned int row, unsigned int collum)
	{
		//check if no limmits are being violated
		if (option_matrix.size() > row)
			if(option_matrix[row].size() <= collum) 
				return false;
		
		option_matrix[row][collum] = new option_type{pos,data,call};
	}

	int newCursor(Sprite* s, const Vec2 &offset, unsigned int start_row, unsigned int start_collum)
	{
		cursors.push_back(new cursor_type{ s,offset,option_matrix[start_row][start_collum],start_row,start_collum});

		cursor_type *c = cursors.at((cursors.size() - 1));

		c->sprite->setPosition(option_matrix[start_row][start_collum]->pos);

		return cursors.size() - 1;
	}

	void incRow(unsigned int cursor_index)
	{
		if (cursors.size() <= cursor_index) { return; }
		
		cursor_type *c = cursors.at(cursor_index);
		int row = ((c->row + 1) == option_matrix.size()) ? 0 : c->row + 1;

		if (option_matrix[row][c->collum] == nullptr) {
			c->print();
			return;
		}

		c->row = row;

		c->current_opt = option_matrix[c->row][c->collum];
		c->print();
	}

	void decRow(unsigned int cursor_index)
	{
		if (cursors.size() <= cursor_index) { return; }

		cursor_type *c = cursors.at(cursor_index);
		int row = (c->row == 0) ? option_matrix.size() -1 : c->row - 1;

		if (option_matrix[row][c->collum] == nullptr) {
			c->print();
			return;
		}

		c->row = row;

		c->current_opt = option_matrix[c->row][c->collum];
		c->print();
	}

	void incCollum(unsigned int cursor_index)
	{
		if (cursors.size() <= cursor_index) { return; }

		cursor_type *c = cursors.at(cursor_index);
		int collum = ((c->collum + 1) == option_matrix[c->row].size()) ? 0 : c->collum + 1;

		if (option_matrix[c->row][collum] == nullptr) {
			c->print();
			return;
		}

		c->collum = collum;


		c->current_opt = option_matrix[c->row][c->collum];
		c->print();
	}

	void decCollum(unsigned int cursor_index)
	{
		if (cursors.size() <= cursor_index) { return; }

		cursor_type *c = cursors.at(cursor_index);
		int collum = (c->collum == 0) ? option_matrix[c->row].size() - 1 : c->collum - 1;

		if (option_matrix[c->row][collum] == nullptr) {
			c->print();
			return;
		}

		c->collum = collum;

		c->current_opt = option_matrix[c->row][c->collum];
		c->print();
	}

	void * getCursorData(unsigned int cursor_index) 
	{
		if (cursors.size() <= cursor_index) { return nullptr; }

		return cursors[cursor_index]->current_opt->data;
	}

	typedef void(T::*call_pointer)();
	call_pointer getCursorCall(unsigned int cursor_index)
	{
		if (cursors.size() <= cursor_index) { return nullptr; }
		return cursors[cursor_index]->current_opt->call;
	}

	Vec2 getCursorPos(unsigned int cursor_index)
	{
		if (cursors.size() <= cursor_index) { return Vec2(0,0); }
		return Vec2(cursors[cursor_index]->row, cursors[cursor_index]->collum);
	}

	void updateCursor(unsigned int cursor_index)
	{
		cursors[cursor_index]->print();
	}

private:

	typedef struct {
		Sprite *sprite;
		Vec2 offset;
		option_type *current_opt;
		unsigned int row;
		unsigned int collum;
		void print() {
			this->sprite->setPosition(this->current_opt->pos + this->offset);
		}
	}cursor_type;

	std::vector<cursor_type*> cursors;

	std::vector<std::vector<option_type *>>option_matrix;

	unsigned int max_rows;
	unsigned int max_collums;
};

#endif