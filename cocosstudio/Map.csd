<GameFile>
  <PropertyGroup Name="Map" Type="Scene" ID="f659d5d3-43c9-4309-9c28-cd31dd4d4980" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="128" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Background" ActionTag="565107936" Tag="133" IconVisible="False" TopMargin="1.1652" BottomMargin="-1.1652" Scale9Width="1920" Scale9Height="1080" ctype="ImageViewObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position Y="-1.1652" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="-0.0011" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="World Map/WorldMap_Background.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_0" ActionTag="-1442966052" Tag="135" IconVisible="False" LeftMargin="641.1440" RightMargin="1118.8560" TopMargin="-19.5479" BottomMargin="999.5479" TouchEnable="True" FontSize="14" Scale9Enable="True" Scale9Width="160" Scale9Height="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="160.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="721.1440" Y="1049.5479" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3756" Y="0.9718" />
            <PreSize X="0.0833" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="World Map/WordMap_Spot.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_1" ActionTag="-464770820" Tag="136" IconVisible="False" LeftMargin="60.2902" RightMargin="1699.7098" TopMargin="241.5221" BottomMargin="738.4779" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="10" BottomEage="10" Scale9OriginX="15" Scale9OriginY="10" Scale9Width="130" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="160.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="140.2902" Y="788.4779" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0731" Y="0.7301" />
            <PreSize X="0.0833" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="World Map/WordMap_Spot.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_2" ActionTag="-1164253594" Tag="137" IconVisible="False" LeftMargin="1489.3298" RightMargin="270.6702" TopMargin="239.2058" BottomMargin="740.7942" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="10" BottomEage="10" Scale9OriginX="15" Scale9OriginY="10" Scale9Width="130" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="160.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1569.3298" Y="790.7942" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8174" Y="0.7322" />
            <PreSize X="0.0833" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="World Map/WordMap_Spot.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_3" ActionTag="365509499" Tag="138" IconVisible="False" LeftMargin="1060.1752" RightMargin="699.8248" TopMargin="617.5169" BottomMargin="362.4831" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="10" BottomEage="10" Scale9OriginX="15" Scale9OriginY="10" Scale9Width="130" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="160.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1140.1752" Y="412.4831" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5938" Y="0.3819" />
            <PreSize X="0.0833" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="World Map/WordMap_Spot.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1_4" ActionTag="975549979" Tag="139" IconVisible="False" LeftMargin="480.6089" RightMargin="1279.3911" TopMargin="879.5104" BottomMargin="100.4896" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="10" BottomEage="10" Scale9OriginX="15" Scale9OriginY="10" Scale9Width="130" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="160.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.4517" ScaleY="0.4392" />
            <Position X="552.8809" Y="144.4096" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2880" Y="0.1337" />
            <PreSize X="0.0833" Y="0.0926" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Normal" Path="World Map/WordMap_Spot.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="map_cursor" ActionTag="275550513" Tag="154" IconVisible="False" LeftMargin="96.1712" RightMargin="1733.8287" TopMargin="162.4545" BottomMargin="797.5455" ctype="SpriteObjectData">
            <Size X="90.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5301" ScaleY="0.5002" />
            <Position X="143.8802" Y="857.5695" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0749" Y="0.7940" />
            <PreSize X="0.0469" Y="0.1111" />
            <FileData Type="Normal" Path="World Map/WordMap_Cursor.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>