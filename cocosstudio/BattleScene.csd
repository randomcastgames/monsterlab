<GameFile>
  <PropertyGroup Name="BattleScene" Type="Scene" ID="b256f582-4ea2-4e7c-813b-ea4a184540d9" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="15" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-1676527084" Tag="78" IconVisible="False" Scale9Width="1920" Scale9Height="1080" ctype="ImageViewObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Scenarios/Scenario_Forest.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BattleScene_HUD_1" ActionTag="-300537690" Tag="14" IconVisible="False" LeftMargin="39.0065" RightMargin="1070.9934" TopMargin="30.5002" BottomMargin="369.4997" ctype="SpriteObjectData">
            <Size X="810.0000" Y="680.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="444.0065" Y="709.4997" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2313" Y="0.6569" />
            <PreSize X="0.4219" Y="0.6296" />
            <FileData Type="Normal" Path="BattleScene_HUD.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BattleScene_HUD_1_0" ActionTag="177015199" Tag="16" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="1078.9875" RightMargin="31.0125" TopMargin="30.5000" BottomMargin="369.5000" FlipX="True" ctype="SpriteObjectData">
            <Size X="810.0000" Y="680.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1483.9875" Y="709.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7729" Y="0.6569" />
            <PreSize X="0.4219" Y="0.6296" />
            <FileData Type="Normal" Path="BattleScene_HUD.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="AttackBar_1" ActionTag="1058720759" Tag="21" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="99.9340" RightMargin="1790.0660" TopMargin="242.7175" BottomMargin="807.2825" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="114.9340" Y="822.2825" />
            <Scale ScaleX="3.7000" ScaleY="3.7000" />
            <CColor A="255" R="18" G="18" B="244" />
            <PrePosition X="0.0599" Y="0.7614" />
            <PreSize X="0.0156" Y="0.0278" />
            <ImageFileData Type="Normal" Path="battleui/cpbar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="AttackBar_2" ActionTag="-1280448012" Tag="22" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="101.5415" RightMargin="1788.4585" TopMargin="376.0302" BottomMargin="673.9698" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="116.5415" Y="688.9698" />
            <Scale ScaleX="3.7000" ScaleY="3.7000" />
            <CColor A="255" R="18" G="18" B="244" />
            <PrePosition X="0.0607" Y="0.6379" />
            <PreSize X="0.0156" Y="0.0278" />
            <ImageFileData Type="Normal" Path="battleui/cpbar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="AttackBar_3" ActionTag="-1360318375" Tag="20" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="101.8359" RightMargin="1788.1641" TopMargin="506.0358" BottomMargin="543.9642" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="116.8359" Y="558.9642" />
            <Scale ScaleX="3.7000" ScaleY="3.7000" />
            <CColor A="255" R="18" G="18" B="244" />
            <PrePosition X="0.0609" Y="0.5176" />
            <PreSize X="0.0156" Y="0.0278" />
            <ImageFileData Type="Normal" Path="cpbar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="AttackBar_4" ActionTag="1428033752" Tag="23" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="104.7900" RightMargin="1785.2100" TopMargin="633.3457" BottomMargin="416.6543" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="119.7900" Y="431.6543" />
            <Scale ScaleX="3.7000" ScaleY="3.7000" />
            <CColor A="255" R="18" G="18" B="244" />
            <PrePosition X="0.0624" Y="0.3997" />
            <PreSize X="0.0156" Y="0.0278" />
            <ImageFileData Type="Normal" Path="cpbar.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="EnemyLifeBar" ActionTag="-1476227465" Tag="25" IconVisible="False" LeftMargin="1317.1497" RightMargin="402.8503" TopMargin="97.8149" BottomMargin="968.1851" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="200.0000" Y="14.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1417.1497" Y="975.1851" />
            <Scale ScaleX="3.1800" ScaleY="3.7400" />
            <CColor A="255" R="18" G="242" B="19" />
            <PrePosition X="0.7381" Y="0.9029" />
            <PreSize X="0.1042" Y="0.0130" />
            <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="PlayerLifeBar" ActionTag="1835009659" Tag="24" IconVisible="False" LeftMargin="409.6116" RightMargin="1310.3884" TopMargin="97.6124" BottomMargin="968.3876" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="200.0000" Y="14.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="509.6116" Y="975.3876" />
            <Scale ScaleX="3.1882" ScaleY="3.7451" />
            <CColor A="255" R="18" G="242" B="19" />
            <PrePosition X="0.2654" Y="0.9031" />
            <PreSize X="0.1042" Y="0.0130" />
            <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BattleScene_SkillButton1_4" ActionTag="1825279774" Tag="17" IconVisible="False" LeftMargin="1749.7942" RightMargin="50.2058" TopMargin="201.0862" BottomMargin="758.9138" ctype="SpriteObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1809.7942" Y="818.9138" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9426" Y="0.7583" />
            <PreSize X="0.0625" Y="0.1111" />
            <FileData Type="Normal" Path="BattleScene_NOSkillButton.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BattleScene_SkillButton2_5" ActionTag="1057091801" Tag="18" IconVisible="False" LeftMargin="1749.6316" RightMargin="50.3684" TopMargin="331.4957" BottomMargin="628.5043" ctype="SpriteObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1809.6316" Y="688.5043" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9425" Y="0.6375" />
            <PreSize X="0.0625" Y="0.1111" />
            <FileData Type="Normal" Path="BattleScene_NOSkillButton.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BattleScene_SkillButton3_6" ActionTag="1950644278" Tag="19" IconVisible="False" LeftMargin="1749.1125" RightMargin="50.8875" TopMargin="461.2535" BottomMargin="498.7465" ctype="SpriteObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1809.1125" Y="558.7465" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9422" Y="0.5174" />
            <PreSize X="0.0625" Y="0.1111" />
            <FileData Type="Normal" Path="BattleScene_NOSkillButton.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BattleScene_SkillButton4_7" ActionTag="-1886531624" Tag="20" IconVisible="False" LeftMargin="1749.8420" RightMargin="50.1580" TopMargin="590.9102" BottomMargin="369.0898" ctype="SpriteObjectData">
            <Size X="120.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1809.8420" Y="429.0898" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9426" Y="0.3973" />
            <PreSize X="0.0625" Y="0.1111" />
            <FileData Type="Normal" Path="BattleScene_NOSkillButton.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="AttackBarEnemy" ActionTag="1070995928" Tag="56" RotationSkewX="270.0000" RotationSkewY="270.0000" IconVisible="False" LeftMargin="1798.2134" RightMargin="91.7866" TopMargin="89.6946" BottomMargin="960.3054" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1813.2134" Y="975.3054" />
            <Scale ScaleX="3.7000" ScaleY="3.7000" />
            <CColor A="255" R="18" G="18" B="244" />
            <PrePosition X="0.9444" Y="0.9031" />
            <PreSize X="0.0156" Y="0.0278" />
            <ImageFileData Type="Normal" Path="cpbar.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>