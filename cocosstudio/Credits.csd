<GameFile>
  <PropertyGroup Name="Credits" Type="Scene" ID="3f3a05de-20d7-4ca7-b0f7-6d0a24692f5c" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="65" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Image_5" ActionTag="1837141716" Tag="79" IconVisible="False" LeftMargin="1367.9094" RightMargin="163.0906" TopMargin="179.7168" BottomMargin="360.2832" Scale9Width="389" Scale9Height="540" ctype="ImageViewObjectData">
            <Size X="389.0000" Y="540.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1562.4094" Y="630.2832" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8138" Y="0.5836" />
            <PreSize X="0.2026" Y="0.5000" />
            <FileData Type="Normal" Path="cocos2dx_portrait.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_1" ActionTag="-1949470677" Tag="66" IconVisible="False" LeftMargin="196.6611" RightMargin="1491.3389" TopMargin="192.1985" BottomMargin="655.8015" Scale9Width="232" Scale9Height="232" ctype="ImageViewObjectData">
            <Size X="232.0000" Y="232.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="312.6611" Y="771.8015" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1628" Y="0.7146" />
            <PreSize X="0.1208" Y="0.2148" />
            <FileData Type="Normal" Path="Profile/Cano.PNG" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_2" ActionTag="852682208" Tag="67" IconVisible="False" LeftMargin="198.7650" RightMargin="1489.2351" TopMargin="654.0117" BottomMargin="193.9883" Scale9Width="232" Scale9Height="232" ctype="ImageViewObjectData">
            <Size X="232.0000" Y="232.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="314.7650" Y="309.9883" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1639" Y="0.2870" />
            <PreSize X="0.1208" Y="0.2148" />
            <FileData Type="Normal" Path="Profile/Everton.PNG" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_3" ActionTag="950257451" Tag="68" IconVisible="False" LeftMargin="820.9271" RightMargin="867.0729" TopMargin="650.6304" BottomMargin="197.3696" Scale9Width="232" Scale9Height="232" ctype="ImageViewObjectData">
            <Size X="232.0000" Y="232.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="936.9271" Y="313.3696" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4880" Y="0.2902" />
            <PreSize X="0.1208" Y="0.2148" />
            <FileData Type="Normal" Path="Profile/Pina.PNG" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_4" ActionTag="2086332299" Tag="69" IconVisible="False" LeftMargin="818.6514" RightMargin="869.3486" TopMargin="187.8376" BottomMargin="660.1624" Scale9Width="232" Scale9Height="232" ctype="ImageViewObjectData">
            <Size X="232.0000" Y="232.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="934.6514" Y="776.1624" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4868" Y="0.7187" />
            <PreSize X="0.1208" Y="0.2148" />
            <FileData Type="Normal" Path="Profile/Will.PNG" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="1037314172" Tag="78" IconVisible="False" LeftMargin="1312.3131" RightMargin="91.6869" TopMargin="719.2028" BottomMargin="250.7972" FontSize="48" LabelText="Powered By Cocos2D-X&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="516.0000" Y="110.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1570.3131" Y="305.7972" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8179" Y="0.2831" />
            <PreSize X="0.2688" Y="0.1019" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2" ActionTag="405943255" Tag="30" IconVisible="False" LeftMargin="183.0780" RightMargin="1481.9220" TopMargin="456.7059" BottomMargin="568.2941" FontSize="48" LabelText="Diego Cano" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="255.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="310.5780" Y="595.7941" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1618" Y="0.5517" />
            <PreSize X="0.1328" Y="0.0509" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2_0" ActionTag="145265878" Tag="31" IconVisible="False" LeftMargin="173.0781" RightMargin="1451.9219" TopMargin="924.9182" BottomMargin="100.0818" FontSize="48" LabelText="Everton Vilela" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="295.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.5781" Y="127.5818" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1670" Y="0.1181" />
            <PreSize X="0.1536" Y="0.0509" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2_1" ActionTag="137545482" Tag="32" IconVisible="False" LeftMargin="736.6177" RightMargin="795.3823" TopMargin="458.5238" BottomMargin="566.4762" FontSize="48" LabelText="William Rodrigues" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="388.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="930.6177" Y="593.9762" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4847" Y="0.5500" />
            <PreSize X="0.2021" Y="0.0509" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2_2" ActionTag="1574701189" Tag="34" IconVisible="False" LeftMargin="820.7114" RightMargin="861.2886" TopMargin="929.4642" BottomMargin="95.5358" FontSize="48" LabelText="Lucas Pina" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="238.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="939.7114" Y="123.0358" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4894" Y="0.1139" />
            <PreSize X="0.1240" Y="0.0509" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>