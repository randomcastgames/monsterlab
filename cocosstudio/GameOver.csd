<GameFile>
  <PropertyGroup Name="GameOver" Type="Scene" ID="46b58093-3369-41c7-b707-3b9d1385f20f" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="70" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="958766353" Tag="71" IconVisible="False" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="1890" Scale9Height="1050" ctype="ImageViewObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Title Screen/TitleScreen_Background.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="29065163" VisibleForFrame="False" Tag="72" IconVisible="False" LeftMargin="565.0000" RightMargin="565.0000" TopMargin="765.0000" BottomMargin="245.0000" ctype="SpriteObjectData">
            <Size X="1090.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.0000" Y="280.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2593" />
            <PreSize X="0.4115" Y="0.0648" />
            <FileData Type="Normal" Path="Title Screen/TitleScreen_GameOver.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>