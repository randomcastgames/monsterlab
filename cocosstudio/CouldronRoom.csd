<GameFile>
  <PropertyGroup Name="CouldronRoom" Type="Scene" ID="863c420c-04e5-4d75-9017-35d3c82aafd1" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="26" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Image_2" ActionTag="2035636443" Tag="69" IconVisible="False" LeftMargin="1920.0000" RightMargin="-1920.0000" FlipX="True" Scale9Width="1920" Scale9Height="1080" ctype="ImageViewObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="1920.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Scenario_Fores_RITUALt.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Image_3" CanEdit="False" ActionTag="-363138210" Tag="77" IconVisible="False" Scale9Width="1920" Scale9Height="1080" ctype="ImageViewObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Scenarios/RitualScene_HUD.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item1" ActionTag="1608195990" CallBackType="Click" Tag="31" IconVisible="False" LeftMargin="1333.7330" RightMargin="426.2670" TopMargin="142.1454" BottomMargin="777.8546" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1413.7330" Y="857.8546" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7363" Y="0.7943" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_SlimeDrop.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item3" ActionTag="-1754175111" CallBackType="Click" Tag="32" IconVisible="False" LeftMargin="1707.5135" RightMargin="52.4865" TopMargin="145.2577" BottomMargin="774.7423" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1787.5135" Y="854.7423" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9310" Y="0.7914" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_Fang.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item5" ActionTag="-1759165234" CallBackType="Click" Tag="33" IconVisible="False" LeftMargin="1518.2493" RightMargin="241.7507" TopMargin="323.7107" BottomMargin="596.2893" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1598.2493" Y="676.2893" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8324" Y="0.6262" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_BigEye.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item7" ActionTag="1968647392" CallBackType="Click" Tag="34" IconVisible="False" LeftMargin="1337.7577" RightMargin="422.2423" TopMargin="494.2567" BottomMargin="425.7433" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1417.7577" Y="505.7433" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7384" Y="0.4683" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_BugWings.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item9" ActionTag="336280637" CallBackType="Click" Tag="35" IconVisible="False" LeftMargin="1700.4548" RightMargin="59.5452" TopMargin="499.9576" BottomMargin="420.0424" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1780.4548" Y="500.0424" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9273" Y="0.4630" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_Poison.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item11" ActionTag="851048343" CallBackType="Click" Tag="36" IconVisible="False" LeftMargin="1515.4451" RightMargin="244.5549" TopMargin="673.4793" BottomMargin="246.5207" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1595.4451" Y="326.5207" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8310" Y="0.3023" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_NoseRing.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item13" ActionTag="-794959516" CallBackType="Click" Tag="44" IconVisible="False" LeftMargin="1335.2098" RightMargin="424.7902" TopMargin="868.9805" BottomMargin="51.0195" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1415.2098" Y="131.0195" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7371" Y="0.1213" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_Leaf.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item2" ActionTag="623999298" CallBackType="Click" Tag="37" IconVisible="False" LeftMargin="1515.1945" RightMargin="244.8055" TopMargin="140.9147" BottomMargin="779.0853" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1595.1945" Y="859.0853" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8308" Y="0.7954" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_SnakeSkin.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item4" ActionTag="842993134" CallBackType="Click" Tag="38" IconVisible="False" LeftMargin="1331.9318" RightMargin="428.0682" TopMargin="319.3388" BottomMargin="600.6612" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1411.9318" Y="680.6612" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7354" Y="0.6302" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_BatWing.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item6" ActionTag="-1547770582" CallBackType="Click" Tag="39" IconVisible="False" LeftMargin="1703.4794" RightMargin="56.5206" TopMargin="316.8807" BottomMargin="603.1193" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1783.4794" Y="683.1193" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9289" Y="0.6325" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_Horn.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item8" ActionTag="-642885303" CallBackType="Click" Tag="40" IconVisible="False" LeftMargin="1517.1571" RightMargin="242.8429" TopMargin="496.5803" BottomMargin="423.4197" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1597.1571" Y="503.4197" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8319" Y="0.4661" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_Sting.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item10" ActionTag="1360501915" CallBackType="Click" Tag="41" IconVisible="False" LeftMargin="1334.9252" RightMargin="425.0748" TopMargin="687.7994" BottomMargin="232.2006" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1414.9252" Y="312.2006" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7369" Y="0.2891" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_PlantVines.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="item12" ActionTag="1322341435" CallBackType="Click" Tag="45" IconVisible="False" LeftMargin="1700.5286" RightMargin="59.4714" TopMargin="687.2919" BottomMargin="232.7081" LeftEage="33" RightEage="52" TopEage="52" BottomEage="52" Scale9OriginX="33" Scale9OriginY="52" Scale9Width="75" Scale9Height="56" ctype="ImageViewObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1780.5286" Y="312.7081" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9274" Y="0.2895" />
            <PreSize X="0.0833" Y="0.1481" />
            <FileData Type="Normal" Path="Items/Items_Wood.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd1" ActionTag="665920084" Tag="58" IconVisible="False" LeftMargin="1416.9232" RightMargin="419.0768" TopMargin="244.4039" BottomMargin="812.5961" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1458.9232" Y="824.0961" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7599" Y="0.7631" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd2" ActionTag="-1419059842" Tag="59" IconVisible="False" LeftMargin="1598.7936" RightMargin="237.2064" TopMargin="240.7422" BottomMargin="816.2578" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1640.7936" Y="827.7578" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8546" Y="0.7664" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd3" ActionTag="-1394095851" Tag="60" IconVisible="False" LeftMargin="1794.0583" RightMargin="41.9417" TopMargin="250.4004" BottomMargin="806.5996" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1836.0583" Y="818.0996" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9563" Y="0.7575" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd4" ActionTag="1346374275" Tag="61" IconVisible="False" LeftMargin="1416.6079" RightMargin="419.3921" TopMargin="417.6387" BottomMargin="639.3613" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1458.6079" Y="650.8613" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7597" Y="0.6026" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd5" ActionTag="-1714939981" Tag="62" IconVisible="False" LeftMargin="1603.3673" RightMargin="232.6327" TopMargin="422.0070" BottomMargin="634.9930" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1645.3673" Y="646.4930" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8570" Y="0.5986" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd6" ActionTag="-92370642" Tag="63" IconVisible="False" LeftMargin="1790.1584" RightMargin="45.8416" TopMargin="420.0415" BottomMargin="636.9585" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1832.1584" Y="648.4585" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9542" Y="0.6004" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd7" ActionTag="553454394" Tag="64" IconVisible="False" LeftMargin="1425.0272" RightMargin="410.9728" TopMargin="598.4159" BottomMargin="458.5841" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1467.0272" Y="470.0841" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7641" Y="0.4353" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd8" ActionTag="-20333816" Tag="65" IconVisible="False" LeftMargin="1607.7217" RightMargin="228.2783" TopMargin="599.1532" BottomMargin="457.8468" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1649.7217" Y="469.3468" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8592" Y="0.4346" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd9" ActionTag="-262033972" Tag="66" IconVisible="False" LeftMargin="1786.2230" RightMargin="49.7770" TopMargin="604.4195" BottomMargin="452.5805" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1828.2230" Y="464.0805" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9522" Y="0.4297" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd10" ActionTag="579308856" Tag="67" IconVisible="False" LeftMargin="1419.8685" RightMargin="416.1315" TopMargin="777.6668" BottomMargin="279.3332" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1461.8685" Y="290.8332" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7614" Y="0.2693" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd11" ActionTag="2051802292" Tag="68" IconVisible="False" LeftMargin="1598.8774" RightMargin="237.1226" TopMargin="788.6174" BottomMargin="268.3826" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1640.8774" Y="279.8826" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8546" Y="0.2592" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd12" ActionTag="-692757130" Tag="69" IconVisible="False" LeftMargin="1790.1639" RightMargin="45.8361" TopMargin="781.6197" BottomMargin="275.3803" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1832.1639" Y="286.8803" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9543" Y="0.2656" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="qtd13" ActionTag="-17986307" Tag="70" IconVisible="False" LeftMargin="1421.4473" RightMargin="414.5527" TopMargin="960.8384" BottomMargin="96.1616" TouchEnable="True" FontSize="20" IsCustomSize="True" LabelText="" PlaceHolderText="Text Field" MaxLengthText="10" ctype="TextFieldObjectData">
            <Size X="84.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1463.4473" Y="107.6616" />
            <Scale ScaleX="1.3781" ScaleY="1.3295" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7622" Y="0.0997" />
            <PreSize X="0.0437" Y="0.0213" />
          </AbstractNodeData>
          <AbstractNodeData Name="select_cursor" ActionTag="755628449" Tag="43" IconVisible="False" LeftMargin="1377.0756" RightMargin="432.9244" TopMargin="271.9135" BottomMargin="668.0865" ctype="SpriteObjectData">
            <Size X="110.0000" Y="140.0000" />
            <AnchorPoint ScaleX="0.5385" ScaleY="0.3445" />
            <Position X="1436.3105" Y="716.3165" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7481" Y="0.6633" />
            <PreSize X="0.0573" Y="0.1296" />
            <FileData Type="Normal" Path="Scenarios/RitualScene_Cursor.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Ritual_Result" ActionTag="2012264849" VisibleForFrame="False" Tag="79" IconVisible="False" LeftMargin="11.0016" RightMargin="-11.0016" ctype="SpriteObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="971.0016" Y="540.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5057" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Ritual_Result_Slime.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>